﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using AnalizadorCambiosConversion.Properties;
using BibliotecaAnalisis;
using BibliotecaBD;
using Newtonsoft.Json;

namespace AnalizadorCambiosConversion {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private AnalisisConversionConfig config;
        private BackgroundWorker worker;
        private BackgroundWorker manualWorker;
        private Timer analisisTimer;
        private const int analisisTimerPeriod = 1000 * 60;
        private Analizador analizador;
        private bool analizando = false;

        public delegate void AnalisisStop ();
        public event AnalisisStop OnAnalisisStop;


        public MainWindow () {
            InitializeComponent ();
            InitializeEvents ();
            LoadConfig ();
            FillUI ();
        }

        private void TitleBar_MouseLeftButtonDown (object sender, MouseButtonEventArgs e) {
            base.OnMouseLeftButtonDown (e);
            DragMove ();
        }

        private void BtnClose_Click (object sender, RoutedEventArgs e) {
            SaveConfig ();
            Close ();
        }

        private void BtnAnalisisManual_Click (object sender, RoutedEventArgs e) {
            SaveConfig();
            ToggleAnalisisButton ();
            ToggleManualAnalisisButton();
            ToggleDetenerButton ();
            ToggleAnalisisState ();
            UpdateAnalisisStateLabel ();
            InitializeManualWorker();
            InitializeManualAnalizer();
            manualWorker.RunWorkerAsync();
        }

        private void BtnAnalizar_Click (object sender, RoutedEventArgs e) {
            SaveConfig ();
            ToggleAnalisisButton ();
            ToggleManualAnalisisButton();
            ToggleDetenerButton ();
            ToggleAnalisisState ();
            UpdateAnalisisStateLabel ();
            InitializeWorker();
            InitializeAnalizer ();
            InitializeTimer ();
        }

        private void BtnDetener_Click (object sender, RoutedEventArgs e)
        {
            StopNormal();
            StopManual();
        }

        private void StopManual()
        {
            if (manualWorker == null) return;
            if (manualWorker.IsBusy) return;
            manualWorker.CancelAsync();
            OnAnalisisStop.Invoke();
        }

        private void StopNormal()
        {
            if (worker == null) return;
            if (worker.IsBusy) return;
            worker.CancelAsync();
            StopTimer();
            OnAnalisisStop.Invoke();
        }

        private void Window_Closing (object sender, System.ComponentModel.CancelEventArgs e) {
            var result = Xceed.Wpf.Toolkit.MessageBox.Show ("¿Seguro que quieres cerrar el programa?", "Cuidado", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (result == MessageBoxResult.No || result == MessageBoxResult.None)
                e.Cancel = true;
        }

        private void ManualWorker_RunWorkerCompleted (object sender, RunWorkerCompletedEventArgs e) {
            EndManualWork (e);
        }

        private void ManualWorker_ProgressChanged (object sender, ProgressChangedEventArgs e) {
            UpdateProgressBar (e.UserState.ToString (), e.ProgressPercentage);
        }

        private void ManualWorker_DoWork (object sender, DoWorkEventArgs e) {
            analizador.AnalizeManual ();
        }

        private void AnalisisWorker_RunWorkerCompleted (object sender, RunWorkerCompletedEventArgs e) {
            EndWork (e);
        }

        private void AnalisisWorker_ProgressChanged (object sender, ProgressChangedEventArgs e) {
            UpdateProgressBar (e.UserState.ToString (), e.ProgressPercentage);
        }

        private void AnalisisWorker_DoWork (object sender, DoWorkEventArgs e) {
            analizador.AnalizeActual ((DateTime)e.Argument);
        }

        private void OnAnalisisStopped () {
            ToggleAnalisisState();
            ToggleAnalisisButton();
            ToggleManualAnalisisButton();
            ToggleDetenerButton();
            UpdateAnalisisStateLabel();
        }

        private void EndWork (RunWorkerCompletedEventArgs e)
        {
            ResetProgressBar();
            if (e.Error != null)
            {
                UpdateProgressBarLabel("Error al Analizar");
                Xceed.Wpf.Toolkit.MessageBox.Show($"{e.Error.Message}\n{e.Error.StackTrace}", "Error");
            }
            else if (e.Cancelled)
            {
                UpdateProgressBarLabel("Analisis Cancelado");
            }
        }

        private void EndManualWork(RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UpdateProgressBarLabel("Error al Analizar");
                Xceed.Wpf.Toolkit.MessageBox.Show($"{e.Error.Message}\n{e.Error.StackTrace}", "Error");
            }
            else if (e.Cancelled)
            {
                UpdateProgressBarLabel("Analisis Cancelado");
            }
            OnAnalisisStop.Invoke();
        }

        private void ResetProgressBar()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                PbAnalisis.Value = 0;
            });
        }

        private void UpdateProgressBarLabel (string message) {
            Application.Current.Dispatcher.Invoke (() => LblProgressBar.Text = message);
        }

        private void UpdateProgressBar (string state, int progress) {
            Application.Current.Dispatcher.Invoke (() => {
                LblProgressBar.Text = $"{state}: {progress}%";
                PbAnalisis.Value = progress;
            });
        }

        private void AnalisisTimer_Elapsed (object sender, ElapsedEventArgs e) {
            DateTime ahora = DateTime.Now;
            if (!HayConfiguracionesActuales (ahora)) return;
            if (analizador.Analizando)
                analizador.QueueAnalisis (ahora);
            else
                worker.RunWorkerAsync (ahora);
        }

        private void Num_LostFocus (object sender, RoutedEventArgs e) {
            SaveConfig ();
        }

        private void InitializeAnalizer () {
            analizador = new Analizador (worker, GetAnalisisArguments ());
        }

        private void InitializeManualAnalizer()
        {
            analizador = new Analizador(manualWorker, GetAnalisisArguments());
        }

        private bool HayConfiguracionesActuales (DateTime ahora) {
            return DBUtils.CountConfiguracionesActuales (ahora) > 0;
        }

        private Dictionary<string, object> GetAnalisisArguments () {
            Dictionary<string, object> args = new Dictionary<string, object>();
            Application.Current.Dispatcher.Invoke(() =>
            {
                args = new Dictionary<string, object>{
                    { "pesoActual", NumPesoActual.Value.Value },
                    { "pesoAnterior", NumPesoAnterior.Value.Value },
                    { "pesoSemana1", NumPesoSemana1.Value.Value },
                    { "pesoSemana2", NumPesoSemana2.Value.Value },
                    { "pesoSemana3", NumPesoSemana3.Value.Value },
                    { "pesoSemana4", NumPesoSemana4.Value.Value },
                    { "diasAnalisis", NumDiasAnalisis.Value.Value },
                    { "idCliente", (CbClientes.SelectedItem as ComboBoxItem).Tag },
                    { "filtroErrorConteo", NumFiltroErrorConteo.Value }
                };
            });
            return args;
        }

        private void InitializeEvents () {
            OnAnalisisStop += OnAnalisisStopped;
        }

        private void InitializeWorker () {
            worker = new BackgroundWorker {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };
            worker.DoWork += AnalisisWorker_DoWork;
            worker.ProgressChanged += AnalisisWorker_ProgressChanged;
            worker.RunWorkerCompleted += AnalisisWorker_RunWorkerCompleted;
        }

        private void InitializeManualWorker () {
            manualWorker = new BackgroundWorker () {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            manualWorker.DoWork += ManualWorker_DoWork;
            manualWorker.ProgressChanged += ManualWorker_ProgressChanged;
            manualWorker.RunWorkerCompleted += ManualWorker_RunWorkerCompleted;
        }

        private void InitializeTimer () {
            analisisTimer = new Timer (analisisTimerPeriod);
            analisisTimer.Elapsed += AnalisisTimer_Elapsed;
            analisisTimer.AutoReset = true;
            analisisTimer.Start ();
        }

        private void CreateNewConfigFile () {
            config = new AnalisisConversionConfig {
                PesoActual = 0,
                PesoAnterior = 0,
                PesoSemana1 = 0,
                PesoSemana2 = 0,
                PesoSemana3 = 0,
                PesoSemana4 = 0,
                DiasAnalisis = 0,
                IndiceCliente = 0,
                FiltroErrorConteo = 15
            };
            File.WriteAllText (AppDomain.CurrentDomain.BaseDirectory + "config.json", config.ToJson ());
        }

        private void LoadConfig () {
            try {
                config = AnalisisConversionConfig.FromJson (File.ReadAllText (AppDomain.CurrentDomain.BaseDirectory + "config.json"));
            } catch {
                CreateNewConfigFile ();
            }
        }

        private void SaveConfig () {
            config.PesoActual = NumPesoActual.Value.Value;
            config.PesoAnterior = NumPesoAnterior.Value.Value;
            config.PesoSemana1 = NumPesoSemana1.Value.Value;
            config.PesoSemana2 = NumPesoSemana2.Value.Value;
            config.PesoSemana3 = NumPesoSemana3.Value.Value;
            config.PesoSemana4 = NumPesoSemana4.Value.Value;
            config.DiasAnalisis = (int) NumDiasAnalisis.Value;
            config.IndiceCliente = CbClientes.SelectedIndex;
            config.FiltroErrorConteo = (int)NumFiltroErrorConteo.Value;
            File.WriteAllText (AppDomain.CurrentDomain.BaseDirectory + "config.json", config.ToJson ());
        }

        private void FillUI () {
            NumPesoAnterior.Value = config.PesoAnterior;
            NumPesoActual.Value = config.PesoActual;
            NumPesoSemana1.Value = config.PesoSemana1;
            NumPesoSemana2.Value = config.PesoSemana2;
            NumPesoSemana3.Value = config.PesoSemana3;
            NumPesoSemana4.Value = config.PesoSemana4;
            NumDiasAnalisis.Value = config.DiasAnalisis;
            InitializeCientesComboBox ();
        }

        private void InitializeCientesComboBox () {
            string json = GetClientesJSON ();
            if (json == null) return;

            JsonConvert.DeserializeObject<List<Cliente>> (json).ForEach (cliente => {
                CbClientes.Items.Add (new ComboBoxItem () { Content = cliente.Nombre.FirstCharToUpper(), Tag = cliente.id.ToString () });
            });

            CbClientes.SelectedIndex = config.IndiceCliente;
        }

        private string GetClientesJSON () {
            OeSoporte.ServiceSoapClient client = new OeSoporte.ServiceSoapClient ("ServiceSoap");
            try
            {
                return client.GetListaCliente (Settings.Default.WebServiceKey);
            } catch
            {
                return null;
            }
        }

        private void ToggleAnalisisButton () {
            Application.Current.Dispatcher.Invoke(() =>
            {
                BtnAnalizar.IsEnabled = !BtnAnalizar.IsEnabled;
            });
        }

        private void ToggleManualAnalisisButton()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                BtnAnalisisManual.IsEnabled = !BtnAnalisisManual.IsEnabled;
            });
        }

        private void ToggleDetenerButton () {
            Application.Current.Dispatcher.Invoke(() =>
            {
                BtnDetener.IsEnabled = !BtnDetener.IsEnabled;
            });
        }

        private void UpdateAnalisisStateLabel () {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (analizando) {
                    LblAnalisisState.Foreground = new SolidColorBrush (Colors.Teal);
                    LblAnalisisState.Content = "Analizando";
                } else {
                    LblAnalisisState.Foreground = new SolidColorBrush (Colors.Red);
                    LblAnalisisState.Content = "Detenido";
                }
            });
        }

        private void ToggleAnalisisState () {
            analizando = !analizando;
        }

        private void StopTimer () {
            if(analisisTimer != null) analisisTimer.Stop ();
        }
    }
}