﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliotecaBD;

namespace BibliotecaAnalisis {
    public class AnalisisDeConversion : IAnalisis {
        private Dictionary<string, object> args;
        private BackgroundWorker worker;
        private List<ReporteHellaDataFixer> reportesAnalisis;
        private List<CambiosConversion> cambios;

        public AnalisisDeConversion (Dictionary<string, object> args, BackgroundWorker worker) {
            this.args = args;
            this.worker = worker;
        }

        public IAnalisis AnalyzeActual (DateTime ahora) {
            cambios = CalcularCambiosConversionActual (ahora);
            return this;
        }

        public IAnalisis AnalyzeManual () {
            cambios = CalcularCambiosConversionManual ();
            return this;
        }

        public IAnalisis AnalyzeQueued (List<ConfiguracionAnalisis> cola) {
            cambios = CalcularCambiosconversionEnCola (cola);
            return this;
        }

        public void Upload () {
            UploadCambiosConversion ();
        }

        private List<CambiosConversion> CalcularCambiosConversion (List<ReporteHellaDataFixer> reportes) {
            List<CambiosConversion> cambios = new List<CambiosConversion> ();
            string nombreCliente = DBUtils.GetNombreCliente (reportes[0].idCliente).FirstCharToUpper ();

            foreach (var reporte in reportes) {
                double media = CalcularMedia (reporte), desviacion = CalcularDesviacion (reporte, media);
                cambios.Add (AgregarCambio (reporte, media, desviacion));
                int progress = (int) ((cambios.Count / (double) reportes.Count) * 100);
                worker.ReportProgress (progress, $"Procesando Tasa Conversion: {nombreCliente}");
            }
            //File.WriteAllText("dump.json", JsonConvert.SerializeObject(cambios, Formatting.Indented));
            return cambios;
        }

        private List<CambiosConversion> CalcularCambiosConversionManual () {
            reportesAnalisis = GetReportesAnalisis (int.Parse (args["idCliente"].ToString ()));
            var reportes = GetReportesUltimosNDias ();
            if (reportes.Count == 0) return new List<CambiosConversion> ();
            return new List<CambiosConversion> (CalcularCambiosConversion (reportes));
        }

        private List<CambiosConversion> CalcularCambiosConversionActual (DateTime ahora) {
            List<CambiosConversion> cambios = new List<CambiosConversion> ();
            foreach (var config in DBUtils.GetConfiguracionesActuales (ahora)) {
                reportesAnalisis = GetReportesAnalisis (config.idCliente);
                var reportes = GetReportesUltimosNDias ();
                if (reportes.Count == 0) continue;
                cambios.AddRange (CalcularCambiosConversion (reportes));
            }

            return cambios;
        }

        private List<CambiosConversion> CalcularCambiosconversionEnCola (List<ConfiguracionAnalisis> cola) {
            List<CambiosConversion> cambios = new List<CambiosConversion> ();
            foreach (var config in cola) {
                reportesAnalisis = GetReportesAnalisis (config.idCliente);
                var reportes = GetReportesUltimosNDias ();
                cambios.AddRange (CalcularCambiosConversion (reportes));
            }
            return cambios;
        }

        private List<ReporteHellaDataFixer> GetReportesAnalisis (int idCliente) {
            DateTime hasta = DateTime.Now.Date;
            DateTime desde = new DateTime (hasta.Year, 1, 1).AddYears (-2);
            using (var context = new Entities ()) {
                return context.ReporteHellaDataFixer
                    .Where (r => r.idCliente == idCliente)
                    .Where (r => r.fecha >= desde && r.fecha <= hasta)
                    .ToList ();
            }
        }

        private CambiosConversion AgregarCambio (ReporteHellaDataFixer reporte, double media, double desviacion) {
            return new CambiosConversion {
                conversion = Math.Round (reporte.tasaConversion, 15),
                    media = Math.Round (media, 15),
                    desviacion = Math.Round (desviacion, 15),
                    fechaConversion = reporte.fecha,
                    fechaActualizacion = DateTime.Now,
                    idCliente = reporte.idCliente,
                    idLocal = reporte.idLocal,
                    idEstadoConversion = GetEstadoConversion (media, desviacion, reporte),
                    idEstadoDesviacion = GetEstadoDesviacion (desviacion, media)
            };
        }

        private void UploadCambiosConversion () {
            using (var context = new Entities ()) {
                worker.ReportProgress (0, "Subiendo Cambios Conversion");
                context.CambiosConversion.AddRange (cambios);
                context.SaveChanges ();
                worker.ReportProgress (100, "Subida Completa");
            }
        }

        private int GetEstadoDesviacion (double desviacion, double media) {
            bool normal = desviacion < (media / 2);
            return normal ? 1 : 2;
        }

        private int GetEstadoConversion (double media, double desviacion, ReporteHellaDataFixer reporte) {
            return reporte.fecha.Month == 12 ? GetEstadoConversionDiciembre (media, desviacion, reporte) : GetEstadoConversionNormal (media, desviacion, reporte);
        }

        private int GetEstadoConversionDiciembre (double media, double desviacion, ReporteHellaDataFixer reporte) {
            desviacion = desviacion == 0 ? (media / 3) : desviacion >= (media / 2) ? (media / 2) : desviacion;
            Tuple<double, double> rango = new Tuple<double, double> (media - 2 * desviacion, media + 3 * desviacion);
            bool normal = reporte.tasaConversion >= rango.Item1 && reporte.tasaConversion <= rango.Item2;
            return normal ? 1 : 2;
        }

        private int GetEstadoConversionNormal (double media, double desviacion, ReporteHellaDataFixer reporte) {
            desviacion = desviacion == 0 ? (media / 3) : desviacion >= (media / 2) ? (media / 2) : desviacion;
            Tuple<double, double> rango = new Tuple<double, double> (media - 2 * desviacion, media + 2 * desviacion);
            bool normal = reporte.tasaConversion >= rango.Item1 && reporte.tasaConversion <= rango.Item2;
            return normal ? 1 : 2;
        }

        private double CalcularMedia (ReporteHellaDataFixer reporte) {
            double actual = CalcularMediaAnhoActual (reporte), historica = CalcularMediaHistorica (reporte);
            if (actual == 0 && historica == 0) return GetMediaUltimasDosSemanas (reporte);
            if (actual == 0) return historica;
            if (historica == 0) return actual;
            int pesoActual = (int) args["pesoActual"], pesoHistorico = (int) args["pesoAnterior"];
            return ((pesoHistorico * historica) + (pesoActual * actual)) / (pesoHistorico + pesoActual);
        }

        private double GetMediaUltimasDosSemanas (ReporteHellaDataFixer reporte) {
            DateTime desde = DateTime.Now.Date.AddDays (-7 * 2);
            DateTime hasta = DateTime.Now.Date;
            var reportes = reportesAnalisis
                .Where (r => r.fecha >= desde && r.fecha <= hasta)
                .Where (r => r.idCliente == reporte.idCliente && r.idLocal == reporte.idLocal)
                .Where (r => r.tasaConversion > 0).ToList ();
            double media = reportes.Sum (r => r.tasaConversion) / reportes.Count;
            return double.IsNaN (media) || double.IsInfinity (media) ? 0 : media;
        }

        private double CalcularDesviacion (ReporteHellaDataFixer reporte, double media) {
            var reportesActual = GetReportesAnhoActualFiltrados (reporte);
            var reportesHistorico = GetReportesHistoricosFiltrados (reporte);
            //using (var writer = new StreamWriter("datos.csv"))
            //using (var csv = new CsvWriter(writer))
            //{
            //    var nuevo = reportesActual.Select(r => r).ToList();
            //    nuevo.AddRange(reportesHistorico);
            //    csv.WriteRecords(nuevo);
            //}
            if (reportesActual.Count == 0 && reportesHistorico.Count == 0) return GetDesviacionUltimasDosSemanas (reporte, media);
            double sumatoria = reportesActual.Sum (r => Math.Pow (r.tasaConversion - media, 2)) + reportesHistorico.Sum (r => Math.Pow (r.tasaConversion - media, 2));
            double desviacion = Math.Sqrt (sumatoria / (reportesActual.Count + reportesHistorico.Count - 1));
            return double.IsNaN (desviacion) || double.IsInfinity (desviacion) ? 0 : desviacion;
        }

        private double GetDesviacionUltimasDosSemanas (ReporteHellaDataFixer reporte, double media) {
            DateTime desde = DateTime.Now.Date.AddDays (-7 * 2);
            DateTime hasta = DateTime.Now.Date;
            var reportes = reportesAnalisis
                .Where (r => r.fecha >= desde && r.fecha <= hasta)
                .Where (r => r.idCliente == reporte.idCliente && r.idLocal == reporte.idLocal)
                .Where (r => r.tasaConversion > 0).ToList ();
            double sumatoria = reportes.Sum (r => Math.Pow (r.tasaConversion - media, 2));
            double desviacion = Math.Sqrt (sumatoria / reportes.Count);
            return Double.IsNaN (desviacion) || Double.IsInfinity (desviacion) ? 0 : desviacion;
        }

        private double CalcularMediaAnhoActual (ReporteHellaDataFixer reporte) {
            var reportes = GetReportesAnhoActualFiltrados (reporte);
            var agrupados = AgruparReportesPorSemana (reportes);
            double media = CalcularNumeradorAnhoActual (agrupados) / CalcularDenominadorAnhoActual (agrupados);
            return double.IsNaN (media) ? 0 : media;
        }

        private double CalcularDenominadorAnhoActual (List<List<ReporteHellaDataFixer>> agrupados) {
            List<int> pesos = GetPesosSemanales ();
            return pesos[0] * agrupados[0].Count + pesos[1] * agrupados[1].Count + pesos[2] * agrupados[2].Count + pesos[3] * agrupados[3].Count;
        }

        private double CalcularNumeradorAnhoActual (List<List<ReporteHellaDataFixer>> agrupados) {
            List<int> pesos = GetPesosSemanales ();
            return agrupados[0].Sum (a => a.tasaConversion * pesos[0]) +
                agrupados[1].Sum (a => a.tasaConversion * pesos[1]) +
                agrupados[2].Sum (a => a.tasaConversion * pesos[2]) +
                agrupados[3].Sum (a => a.tasaConversion * pesos[3]);
        }

        private List<int> GetPesosSemanales () {
            return new List<int> {
                (int) args["pesoSemana1"],
                (int) args["pesoSemana2"],
                (int) args["pesoSemana3"],
                (int) args["pesoSemana4"]
            };
        }

        private List<ReporteHellaDataFixer> GetReportesAnhoActualFiltrados (ReporteHellaDataFixer reporte) {
            var reportes = GetReportesAnhoActual (reporte);
            reportes = FiltrarReportesPorDia (reportes, reporte.fecha);
            reportes = FiltrarReportesPorErrorConteo (reportes);
            reportes = FiltrarReportesPorConversionMayorACero (reportes);
            reportes = FiltrarReportesPorUltimasNSemanas (reportes, 12);
            return reportes;
        }

        private List<List<ReporteHellaDataFixer>> AgruparReportesPorSemana (List<ReporteHellaDataFixer> reportes) {
            return new List<List<ReporteHellaDataFixer>> {
                GetReportesFiltradosPorSemana (new int[] { 1 }, reportes),
                GetReportesFiltradosPorSemana (new int[] { 2, 3 }, reportes),
                GetReportesFiltradosPorSemana (new int[] { 4, 7 }, reportes),
                GetReportesFiltradosPorSemana (new int[] { 8, 13 }, reportes),
            };
        }

        private List<ReporteHellaDataFixer> GetReportesFiltradosPorSemana (int[] intervalo, List<ReporteHellaDataFixer> reportes) {
            DateTime desde = intervalo.Length == 1 ? DateTime.Now.Date.AddDays (-7) : DateTime.Now.Date.AddDays (-intervalo[0] * 7);
            DateTime hasta = intervalo.Length == 1 ? DateTime.Now.Date : DateTime.Now.Date.AddDays (-intervalo[intervalo.Length - 1] * 7);
            return reportes.Where (r => r.fecha >= desde && r.fecha >= hasta).ToList ();
        }

        private List<ReporteHellaDataFixer> FiltrarReportesPorUltimasNSemanas (List<ReporteHellaDataFixer> reportes, int semanas) {
            DateTime hasta = DateTime.Now.Date;
            DateTime desde = hasta.AddDays (-(semanas * 7));
            return reportes.Where (r => r.fecha >= desde && r.fecha <= hasta).ToList ();
        }

        private List<ReporteHellaDataFixer> FiltrarReportesPorConversionMayorACero (List<ReporteHellaDataFixer> reportes) {
            return reportes.Where (r => r.tasaConversion > 0).ToList ();
        }

        private List<ReporteHellaDataFixer> FiltrarReportesPorErrorConteo (List<ReporteHellaDataFixer> reportes) {
            return reportes.Where (r => r.errorConteo < (int) args["filtroErrorConteo"] || r.errorConteo == null).ToList ();
        }

        private List<ReporteHellaDataFixer> FiltrarReportesPorDia (List<ReporteHellaDataFixer> reportes, DateTime fecha) {
            return reportes.Where (r => (int) r.fecha.DayOfWeek == (int) fecha.DayOfWeek).ToList ();
        }

        private double CalcularMediaHistorica (ReporteHellaDataFixer reporte) {
            var reportes = GetReportesHistoricosFiltrados (reporte);
            var agrupados = AgruparReportesPorAnho (reportes, reporte.fecha);
            double media = CalcularNumeradorHistorico (agrupados) / CalcularDenominadorHistorico (agrupados);
            return double.IsNaN (media) ? 0 : media;
        }

        private double CalcularNumeradorHistorico (List<List<ReporteHellaDataFixer>> agrupados) {
            return agrupados[0].Sum (r => r.tasaConversion) + agrupados[1].Sum (r => r.tasaConversion * 2);
        }

        private double CalcularDenominadorHistorico (List<List<ReporteHellaDataFixer>> agrupados) {
            return agrupados[0].Count () + agrupados[1].Count * 2;
        }

        private List<List<ReporteHellaDataFixer>> AgruparReportesPorAnho (List<ReporteHellaDataFixer> reportes, DateTime fecha) {
            DateTime diaUnoAnhoActual = new DateTime (fecha.Year, 1, 1);
            return new List<List<ReporteHellaDataFixer>> {
                reportes.Where (r => r.fecha >= diaUnoAnhoActual.AddYears (-1) && r.fecha < diaUnoAnhoActual).ToList (),
                reportes.Where (r => r.fecha >= diaUnoAnhoActual.AddYears (-2) && r.fecha < diaUnoAnhoActual.AddYears (-1)).ToList ()
            };
        }

        private List<ReporteHellaDataFixer> GetReportesHistoricosFiltrados (ReporteHellaDataFixer report) {
            var reportes = GetReportesHistoricos (report);
            reportes = GetReportesHistoricosFiltradosPorMes (reportes, report.fecha);
            reportes = GetReportesHistoricosFiltradosPorDia (reportes, report.fecha);
            reportes = FiltrarReportesPorErrorConteo (reportes);
            reportes = FiltrarReportesPorConversionMayorACero (reportes);
            return reportes;
        }

        private List<ReporteHellaDataFixer> GetReportesHistoricosFiltradosPorDia (List<ReporteHellaDataFixer> reportes, DateTime fecha) {
            return reportes.Where (r => DateUtils.ObtenerDiasAnalisis (fecha).Contains (r.fecha.DayOfWeek)).ToList ();
        }

        private List<ReporteHellaDataFixer> GetReportesHistoricosFiltradosPorMes (List<ReporteHellaDataFixer> reportes, DateTime fecha) {
            return reportes.Where (r => DateUtils.ObtenerMesesAnalisis (fecha).Contains (r.fecha.Month)).ToList ();
        }

        private List<ReporteHellaDataFixer> GetReportesHistoricos (ReporteHellaDataFixer report) {
            DateTime hasta = new DateTime (report.fecha.Year, 1, 1);
            DateTime desde = hasta.AddYears (-2);
            return reportesAnalisis
                .Where (r => r.idCliente == report.idCliente && r.idLocal == report.idLocal)
                .Where (r => r.fecha >= desde && r.fecha <= hasta)
                .ToList ();
        }

        private List<ReporteHellaDataFixer> GetReportesAnhoActual (ReporteHellaDataFixer report) {
            DateTime diaUno = new DateTime (report.fecha.Year, 1, 1);
            return reportesAnalisis
                .Where (r => r.idCliente == report.idCliente && r.idLocal == report.idLocal)
                .Where (r => r.fecha <= report.fecha && r.fecha >= diaUno)
                .ToList ();
        }

        private List<ReporteHellaDataFixer> GetReportesUltimosNDias () {
            DateTime hasta = DateTime.Now.Date;
            DateTime desde = hasta.AddDays (-int.Parse (args["diasAnalisis"].ToString ()));
            return reportesAnalisis
                .Where (r => r.fecha <= hasta && r.fecha >= desde)
                .ToList ();
        }
    }
}