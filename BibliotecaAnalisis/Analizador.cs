﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliotecaBD;
using CsvHelper;
using Newtonsoft.Json;

namespace BibliotecaAnalisis {
    public class Analizador {
        private AnalisisDeConversion conversion;
        private AnalisisDeCaptura captura;
        private List<ReporteHellaDataFixer> reportesAnalisis;

        public Queue<TimeSpan> AnalisisEnEspera { get; set; }
        public bool Analizando { get; set; }

        public Analizador () { }
        public Analizador (BackgroundWorker worker, Dictionary<string, object> args) {
            this.conversion = new AnalisisDeConversion (args, worker);
            this.captura = new AnalisisDeCaptura (args, worker);
            this.AnalisisEnEspera = new Queue<TimeSpan> ();
        }

        public void AnalizeActual (DateTime ahora) {
            Analizando = true;
            conversion.AnalyzeActual (ahora).Upload ();
            captura.AnalyzeActual(ahora).Upload();
            Analizando = false;
            if (AnalisisEnEspera.Count > 0) AnalizeQueued ();
        }

        private void AnalizeQueued () {
            Analizando = true;
            conversion.AnalyzeQueued (GetQueuedConfigurations ()).Upload ();
            captura.AnalyzeQueued(GetQueuedConfigurations()).Upload();
            Analizando = false;
            if (AnalisisEnEspera.Count > 0) AnalizeQueued ();
        }

        public void AnalizeManual () {
            Analizando = true;
            conversion.AnalyzeManual ().Upload ();
            captura.AnalyzeManual().Upload();
            Analizando = false;
        }

        public void QueueAnalisis (DateTime horaAnalisis) {
            if (AnalisisEnEspera == null) AnalisisEnEspera = new Queue<TimeSpan> ();
            AnalisisEnEspera.Enqueue (horaAnalisis.TimeOfDay);
        }

        private List<ConfiguracionAnalisis> GetQueuedConfigurations () {
            using (var context = new Entities ()) {
                List<TimeSpan> horas = AnalisisEnEspera.Select (a => new TimeSpan (a.Hours, a.Minutes, 0)).ToList ();
                AnalisisEnEspera.Clear ();
                return context.ConfiguracionAnalisis.Where (c => horas.Contains (c.horaProceso)).ToList ();
            }
        }
    }
}