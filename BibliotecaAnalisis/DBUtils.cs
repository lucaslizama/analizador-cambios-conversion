using System;
using System.Collections.Generic;
using System.Linq;
using BibliotecaBD;

public class DBUtils {
    /// <summary>
    /// Este metodo trae las ConfiguracionesAnalisisConversion para
    /// el minuto actual.
    /// </summary>
    /// <returns></returns>
    public static List<ConfiguracionAnalisis> GetConfiguracionesActuales (DateTime now) {
        TimeSpan ahora = now.TimeOfDay;
        TimeSpan hora = new TimeSpan (ahora.Hours, ahora.Minutes, 0);
        using (var context = new Entities ()) {
            return context.ConfiguracionAnalisis.Where (c => c.horaProceso == hora).ToList ();
        }
    }

    /// <summary>
    /// Este metodo cuentas configuraciones existen para el minuto actual
    /// </summary>
    /// <returns></returns>
    public static int CountConfiguracionesActuales (DateTime ahora) {
        using (var context = new Entities ()) {
            return GetConfiguracionesActuales (ahora).Count ();
        }
    }

    /// <summary>
    /// Metodo que retorna el nombre de un cliente segun su idCliente
    /// </summary>
    /// <returns>El nombre del cliente</returns>
    public static string GetNombreCliente (int idCliente) {
        using (var context = new Entities ()) {
            return context.Cliente.Where (c => c.id == idCliente).First ().Nombre;
        }
    }
}