using System;
using System.Collections.Generic;

public class DateUtils {
    public static List<int> ObtenerMesesAnalisis (DateTime fechaConversion) {
        switch (fechaConversion.Month) {
            case 1:
            case 2:
            case 3:
                return new List<int> { 1, 2, 3 };
            case 4:
            case 5:
            case 6:
                return new List<int> { 4, 5, 6 };
            case 7:
            case 8:
            case 9:
                return new List<int> { 7, 8, 9 };
            case 10:
            case 11:
                return new List<int> { 10, 11 };
            case 12:
                return new List<int> { 12 };
            default:
                return new List<int> ();
        }
    }

    public static List<DayOfWeek> ObtenerDiasAnalisis (DateTime fechaConversion) {
        switch (fechaConversion.DayOfWeek) {
            case DayOfWeek.Monday:
            case DayOfWeek.Tuesday:
            case DayOfWeek.Wednesday:
            case DayOfWeek.Thursday:
            case DayOfWeek.Friday:
                return new List<DayOfWeek> { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };
            case DayOfWeek.Saturday:
                return new List<DayOfWeek> { DayOfWeek.Saturday };
            case DayOfWeek.Sunday:
                return new List<DayOfWeek> { DayOfWeek.Sunday };
            default:
                return new List<DayOfWeek> ();
        }
    }
}