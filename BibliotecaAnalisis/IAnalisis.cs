using System;
using System.Collections.Generic;
using BibliotecaBD;

public interface IAnalisis {
    IAnalisis AnalyzeActual (DateTime ahora);
    IAnalisis AnalyzeQueued (List<ConfiguracionAnalisis> cola);
    IAnalisis AnalyzeManual ();
    void Upload ();
}